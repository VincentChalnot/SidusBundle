<?php

namespace Sidus\SidusBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Sidus\SidusBundle\Exception\AccessDeniedException;
use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\UserRepository")
 */
class User extends Object implements UserInterface, Serializable {
	
	public static $reservedProperties = ['password', 'oldPassword', 'salt', 'confirmationToken', 'passwordRequestedAt'];

	/**
	 * @var string
	 * @ORM\Column(name="username", unique=true, type="string", length=255)
	 * @Assert\NotBlank()
	 */
	private $username;

	/**
	 * @var string
	 * @ORM\Column(name="password", type="string", length=41)
	 * @Assert\NotBlank()
	 */
	private $password;
	
	/**
	 * Used when changing password
	 * @var string
	 */
	private $oldPassword;

	/**
	 * Plain text password when setting the password
	 * @var string
	 */
	private $plainTextPassword;

	/**
	 * @var string
	 * @ORM\Column(name="salt", type="string", length=41)
	 */
	private $salt;

	/**
	 * @var string
	 * @ORM\Column(name="email", type="string", length=255)
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;

	/**
	 * @var \Datetime
	 * @ORM\Column(name="expires_at", type="datetime", nullable=true)
	 * @Assert\DateTime()
	 */
	private $expiresAt;

	/**
	 * @var \Datetime
	 * @ORM\Column(name="last_login", type="datetime", nullable=true)
	 * @Assert\DateTime()
	 */
	private $lastLogin;

	/**
	 * Random string sent to the user email address in order to verify it
	 *
	 * @var string
	 * @ORM\Column(name="confirmation_token", type="string", length=41, nullable=true)
	 */
	private $confirmationToken;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
	 * @Assert\DateTime()
	 */
	private $passwordRequestedAt;

	/**
	 * @var boolean
	 * @ORM\Column(name="enabled", type="boolean")
	 */
	private $enabled;

	/**
	 * @var string
	 * @ORM\Column(name="role", type="string", length=64)
	 * @Assert\NotBlank()
	 */
	private $role;

	public function __construct() {
		parent::__construct();
		$this->enabled = true;
		$this->lastLogin = new \DateTime;
		$this->role = 'ROLE_USER';
	}

	/**
	 * @param string $username
	 * @return User
	 */
	public function setUsername($username) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::OWNER);
		$this->username = $username;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername() {
		return $this->checkPermission(__FUNCTION__, $this->username);
	}

	/**
	 * Dummy function to prevent manual setting of the password
	 *
	 * @param string $password
	 * @return User
	 */
	public function setPassword($password) {
		$this->setPlainTextPassword($password);
		//throw new AccessDeniedException('You should\'t set the password manually, please use setPlainTextPassword instead');
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}
	
	
	/**
	 * @return string
	 */
	public function getOldPassword() {
		return $this->oldPassword;
	}

	/**
	 * @param string $salt
	 * @return User
	 */
	public function setSalt($salt) {
		throw new AccessDeniedException('You should\'t set the salt manually, it\'s already randomly generated');
	}

	/**
	 * @return string
	 */
	public function getSalt() {
		return $this->salt;
	}

	/**
	 * @param DateTime $expiresAt
	 * @return User
	 */
	public function setExpiresAt($expiresAt) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->expiresAt = new DateTime($expiresAt);
		return $this;
	}

	/**
	 * @return DateTime
	 */
	public function getExpiresAt() {
		return $this->checkPermission(__FUNCTION__, $this->expiresAt);
	}

	/**
	 * @param boolean $enabled
	 * @return User
	 */
	public function setEnabled($enabled) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->enabled = $enabled;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function getEnabled() {
		return $this->checkPermission(__FUNCTION__, $this->enabled);
	}

	/**
	 * @param string $email
	 * @return User
	 */
	public function setEmail($email) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::OWNER);
		$this->email = $email;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->checkPermission(__FUNCTION__, $this->email);
	}

	public function eraseCredentials() {
		
	}

	public function setRoles(array $roles) {
		$this->roles = $roles;
		return $this;
	}

	public function getRoles() {
		return array($this->role);
	}

	public function hasRole($role) {
		if (in_array($role, $this->getRoles())) {
			return true;
		}
		return false;
	}

	public function serialize() {
		return serialize($this->getId());
	}

	public function unserialize($data) {
		$this->setId(unserialize($data));
	}

	/**
	 * TODO
	 * @return Object[]
	 */
	public function getGroups() {
		return array();
	}

	/**
	 * @return string
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * @param string $role
	 * @return \Sidus\SidusBundle\Entity\User
	 */
	public function setRole($role) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->role = $role;
		return $this;
	}

	/**
	 * @param \DateTime $lastLogin
	 * @return User
	 */
	public function setLastLogin($lastLogin) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::OWNER);
		$this->lastLogin = $lastLogin;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getLastLogin() {
		return $this->checkPermission(__FUNCTION__, $this->lastLogin);
	}

	/**
	 * @param string $confirmationToken
	 * @return User
	 */
	public function setConfirmationToken($confirmationToken) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->confirmationToken = $confirmationToken;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getConfirmationToken() {
		return $this->checkPermission(__FUNCTION__, $this->confirmationToken, PermissionMask::ADMIN);
	}

	/**
	 * @param \DateTime $passwordRequestedAt
	 * @return User
	 */
	public function setPasswordRequestedAt($passwordRequestedAt) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->passwordRequestedAt = $passwordRequestedAt;
		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getPasswordRequestedAt() {
		return $this->checkPermission(__FUNCTION__, $this->passwordRequestedAt, PermissionMask::OWNER);
	}

	/**
	 * Hash and store the password
	 *
	 * @param string $plainTextPassword
	 * @return \Sidus\SidusBundle\Entity\User
	 */
	public function setPlainTextPassword($plainTextPassword) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::OWNER);
		$this->plainTextPassword = $plainTextPassword;
		if($this->salt === null){
			$this->salt = self::generateSalt();
		}
		$this->oldPassword = $this->password;
		$this->password = $this->encodePassword($plainTextPassword, $this->salt);
		return $this;
	}
	
	public function encodePassword($plainTextPassword, $salt = null){
		if(null === $salt){
			$salt = $this->salt;
		}
		if (null === self::$container) {
			return sha1($plainTextPassword.'{'.$salt.'}');
		} else {
			$encoder = self::$container->get('security.encoder_factory')->getEncoder($this);
			return $encoder->encodePassword($plainTextPassword, $salt);
		}
	}

	/**
	 * Generate random salt for password storage
	 *
	 * @return string
	 */
	public static function generateSalt() {
		return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
	}

	public function getProfile() {
		return 'Lorem ipsum';
	}
	
	public static function getReservedProperties(){
		return array_merge(self::$reservedProperties, parent::getReservedProperties());
	}
	
	public function getFormType() {
		return 'Sidus\\SidusBundle\\Form\\Type\\UserType';
	}

	public function getActions() {
		$actions = parent::getActions();
		$actions['edit']->setPermissionMask(new PermissionMask('owner'));
		unset($actions['move']);
		unset($actions['copy']);
		unset($actions['translate']);
		unset($actions['history']);
		return $actions;
	}
	
}
