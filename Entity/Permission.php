<?php

namespace Sidus\SidusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Object
 *
 * @ORM\Table(name="permission")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\PermissionRepository")
 */
class Permission {

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Node", inversedBy="permissions", cascade={"persist"})
	 * @ORM\JoinColumn(name="node_id", referencedColumnName="id")
	 */
	protected $node;

	/**
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Object", inversedBy="authorizations", cascade={"persist"})
	 * @ORM\JoinColumn(name="object_id", referencedColumnName="id")
	 */
	protected $entity;

	/**
	 * Bitmask corresponding to:
	 * 1: Read
	 * 2: Write
	 * 4: Add
	 * 8: Owner
	 * 16: Master
	 *
	 * @var integer
	 * @ORM\Column(name="authorizations", type="integer")
	 * @Assert\NotBlank()
	 */
	protected $authorizations;

	/**
	 * See authorization bitmask
	 *
	 * @var integer
	 * @ORM\Column(name="restrictions", type="integer")
	 * @Assert\NotBlank()
	 */
	protected $restrictions;

	public function __construct() {
		$this->authorizations = 0;
		$this->restrictions = 0;
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set authorizations
	 *
	 * @param PermissionMask $authorizations
	 * @return Permission
	 */
	public function setAuthorizations(PermissionMask $authorizations) {
		$this->authorizations = $authorizations->getMask();
		return $this;
	}

	/**
	 * Get authorizations
	 *
	 * @return PermissionMask
	 */
	public function getAuthorizations() {
		return new PermissionMask($this->authorizations);
	}

	/**
	 * Set restrictions
	 *
	 * @param PermissionMask $restrictions
	 * @return Permission
	 */
	public function setRestrictions(PermissionMask $restrictions) {
		$this->restrictions = $restrictions->getMask();
		return $this;
	}

	/**
	 * Get restrictions
	 *
	 * @return PermissionMask
	 */
	public function getRestrictions() {
		return new PermissionMask($this->restrictions);
	}

	/**
	 * Set node
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $node
	 * @return Permission
	 */
	public function setNode(\Sidus\SidusBundle\Entity\Node $node = null) {
		$this->node = $node;
		return $this;
	}

	/**
	 * Get node
	 *
	 * @return \Sidus\SidusBundle\Entity\Node
	 */
	public function getNode() {
		return $this->node;
	}

	/**
	 * Set entity
	 *
	 * @param \Sidus\SidusBundle\Entity\Object $entity
	 * @return Permission
	 */
	public function setEntity(\Sidus\SidusBundle\Entity\Object $entity = null) {
		$this->entity = $entity;
		return $this;
	}

	/**
	 * Get entity
	 *
	 * @return \Sidus\SidusBundle\Entity\Object
	 */
	public function getEntity() {
		return $this->entity;
	}

	/**
	 * Magic methods used by permissions form
	 * @param string $name
	 * @param mixed $arguments
	 * @return boolean
	 */
	public function __get($name) {
		if(substr($name, 0, 5) == 'allow') {
			$key = strtolower(substr($name, 5));
			if(isset(PermissionMask::$masks[$key])){
				return $this->getAuthorizations()->check(PermissionMask::$masks[$key]);
			}
		}
		if(substr($name, 0, 6) == 'forbid') {
			$key = substr($name, 6);
			if(isset(PermissionMask::$masks[$key])){
				return $this->getRestrictions()->check(PermissionMask::$masks[$key]);
			}
		}
		return false;
	}
	
	/**
	 * Magic methods used by permissions form
	 * @param string $name
	 * @param mixed $arguments
	 * @return boolean
	 */
	public function __set($name, $value) {
		if(substr($name, 0, 5) == 'allow') {
			$key = strtolower(substr($name, 5));
			if(isset(PermissionMask::$masks[$key])){
				$mask = new PermissionMask($this->getAuthorizations()->getMask() | PermissionMask::$masks[$key]);
				$this->setAuthorizations($mask);
			}
		}
		if(substr($name, 0, 6) == 'forbid') {
			$key = substr($name, 6);
			if(isset(PermissionMask::$masks[$key])){
				$mask = new PermissionMask($this->getRestrictions()->getMask() | PermissionMask::$masks[$key]);
				$this->setRestrictions($mask);
			}
		}
		return false;
	}

}
