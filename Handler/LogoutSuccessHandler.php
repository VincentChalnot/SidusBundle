<?php

namespace Sidus\SidusBundle\Handler;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface {

	protected $router;

	public function __construct(Router $router) {
		$this->router = $router;
	}

	public function onLogoutSuccess(Request $request) {
		$redirect = new RedirectResponse($this->router->generate('sidus_show_node', array('node_id' => $request->get('node_id'), 'logout_success' => 1)));
		return $redirect;
	}

}