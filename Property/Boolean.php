<?php

namespace Sidus\SidusBundle\Property;

class Boolean extends GenericProperty {

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::set()
	 * @param boolean $value
	 * @return boolean
	 */
	public function set($value){
		try {
			$value = (boolean)$value;
		} catch(Exception $e){
			return false;
		}
		return parent::set($value);
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::check()
	 * @param boolean $value
	 * @return boolean
	 */
	public function check($value){
		try {
			$value = (boolean)$value;
		} catch(Exception $e){
			return false;
		}
		return true;
	}

}
