<?php

namespace Sidus\SidusBundle\Entity;

use ErrorException;
use Sidus\SidusBundle\OGM\NodeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="node")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\NodeRepository")
 */
class Node {

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="node_name", type="string", length=255, nullable=true)
	 */
	private $nodeName;

	/**
	 * @var Node[]
	 * @ORM\OneToMany(targetEntity="Sidus\SidusBundle\Entity\Node", mappedBy="parent", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=true)
	 * @ORM\OrderBy({"lft" = "ASC"})
	 */
	private $children;

	/**
	 * @var Node
	 * @Gedmo\TreeParent
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Node", inversedBy="children", cascade={"persist"})
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 */
	private $parent;

	/**
	 * @var integer
	 * @ORM\Column(name="object_reference", type="integer", nullable=true)
	 */
	private $objectReference;

	/**
	 * @var Permission[]
	 * @ORM\OneToMany(targetEntity="Sidus\SidusBundle\Entity\Permission", mappedBy="node", cascade={"refresh","remove"})
	 */
	private $permissions;

	/**
	 * @ORM\Column(name="inherit_permissions", type="boolean")
	 */
	private $inheritPermissions;

	/**
	 * Object loaded by the node
	 * @var Object
	 */
	protected $currentObject;

	/**
	 * @var ContainerInterface
	 *
	 * @api
	 */
	protected static $container;

	/**
	 * @var NodeManager
	 */
	protected $nodeManager;

	/**
	 * Sets the Container associated with this Controller.
	 *
	 * @param ContainerInterface $container A ContainerInterface instance
	 *
	 * @api
	 */
	public static function setContainer(ContainerInterface $container = null) {
		self::$container = $container;
	}

	/**
	 * Constructor
	 */
	public function __construct(NodeManager $nodeManager) {
		$this->nodeManager = $nodeManager;
		$this->children = new ArrayCollection();
		$this->inheritPermissions = 1;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set nodeName
	 *
	 * @param string $nodeName
	 * @return Node
	 */
	public function setNodeName($nodeName) {
		$this->nodeName = $nodeName;
		return $this;
	}

	/**
	 * Get nodeName
	 *
	 * @return string
	 */
	public function getNodeName() {
		return $this->nodeName;
	}

	/**
	 * Set parent
	 *
	 * @param Node $parent
	 * @return Node
	 */
	public function setParent(Node $parent) {
		$this->parent = $parent;
		return $this;
	}

	/**
	 * Get parent
	 *
	 * @return Node
	 */
	public function getParent() {
		if ($this->id != 1) {
			return $this->parent;
		} else {
			return null;
		}
	}

	/**
	 *
	 * @return ArrayCollection
	 */
	public function getAscendants() {
		$result = new ArrayCollection(); //Array of ascendants nodes
		$tmp = $this;
		while ($tmp->getParent()) {
			$tmp = $tmp->getParent();
			$result->add($tmp);
		}
		return $result;
	}

	/**
	 * Add child
	 *
	 * @param Node $child
	 * @return Node
	 */
	public function addChild(Node $child) {
		$this->children[] = $child;
		return $this;
	}

	/**
	 * Remove child
	 *
	 * @param Node $child
	 */
	public function removeChild(Node $child) {
		$this->children->removeElement($child);
	}

	/**
	 * Get children
	 *
	 * @return Collection
	 */
	public function getChildren() {
		return $this->children;
	}

	/**
	 *
	 * @return string
	 */
	public function __toString() {
		try {
			return (string) $this->getObject();
		} catch (ErrorException $e) {
			return (string) $this->getNodeName();
		}
	}

	/**
	 * Set lft
	 *
	 * @param integer $lft
	 * @return Node
	 */
	public function setLft($lft) {
		$this->lft = $lft;
		return $this;
	}

	/**
	 * Get lft
	 *
	 * @return integer
	 */
	public function getLft() {
		return $this->lft;
	}

	/**
	 * Set rgt
	 *
	 * @param integer $rgt
	 * @return Node
	 */
	public function setRgt($rgt) {
		$this->rgt = $rgt;
		return $this;
	}

	/**
	 * Get rgt
	 *
	 * @return integer
	 */
	public function getRgt() {
		return $this->rgt;
	}

	/**
	 * Set lvl
	 *
	 * @param integer $lvl
	 * @return Node
	 */
	public function setLvl($lvl) {
		$this->lvl = $lvl;

		return $this;
	}

	/**
	 * Get lvl
	 *
	 * @return integer
	 */
	public function getLvl() {
		return $this->lvl;
	}

	/**
	 * Add children
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $children
	 * @return Node
	 */
	public function addChildren(\Sidus\SidusBundle\Entity\Node $children) {
		$this->children[] = $children;

		return $this;
	}

	/**
	 * Remove children
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $children
	 */
	public function removeChildren(\Sidus\SidusBundle\Entity\Node $children) {
		$this->children->removeElement($children);
	}

	/**
	 * Returns the object that loaded the object
	 * @return Object
	 */
	public function getCurrentObject() {
		if ($this->currentObject) {
			$this->currentObject->setCurrentNode($this);
		}
		return $this->currentObject;
	}

	/**
	 * Alias for getCurrentObject
	 * @return Object
	 */
	public function getObject() {
		if (!$this->currentObject && $this->objectReference) {
			$this->setCurrentObject(self::$container->get('doctrine')
							->getRepository('SidusBundle:Object')
							->findOneByReference($this->objectReference));
		}
		return $this->getCurrentObject();
	}

	/**
	 * Set current object during the loading of the object
	 * @param \Sidus\SidusBundle\Entity\Object $currentObject
	 * @return \Sidus\SidusBundle\Entity\Object
	 */
	public function setCurrentObject(Object $currentObject) {
		$this->currentObject = $currentObject;
		return $this;
	}

	/**
	 * Set inheritPermissions
	 *
	 * @param boolean $inheritPermissions
	 * @return Node
	 */
	public function setInheritPermissions($inheritPermissions) {
		$this->inheritPermissions = $inheritPermissions;

		return $this;
	}

	/**
	 * Get inheritPermissions
	 *
	 * @return boolean
	 */
	public function getInheritPermissions() {
		if (null === $this->getParent()) {
			return false;
		}
		return $this->inheritPermissions;
	}

	/**
	 * Set objectReference
	 *
	 * @param integer $objectReference
	 * @return Node
	 */
	public function setObjectReference($objectReference) {
		$this->objectReference = $objectReference;
		return $this;
	}

	/**
	 * Get objectReference
	 *
	 * @return integer
	 */
	public function getObjectReference() {
		return $this->objectReference;
	}

	/**
	 * Add permissions
	 *
	 * @param \Sidus\SidusBundle\Entity\Permission $permissions
	 * @return Node
	 */
	public function addPermission(\Sidus\SidusBundle\Entity\Permission $permissions) {
		$this->permissions[] = $permissions;
		return $this;
	}

	/**
	 * Remove permissions
	 *
	 * @param \Sidus\SidusBundle\Entity\Permission $permissions
	 */
	public function removePermission(\Sidus\SidusBundle\Entity\Permission $permissions) {
		$this->permissions->removeElement($permissions);
	}

	/**
	 * Get permissions
	 *
	 * @return Collection 
	 */
	public function getPermissions() {
		return $this->permissions;
	}

}
