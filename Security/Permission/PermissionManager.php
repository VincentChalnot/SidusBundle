<?php

namespace Sidus\SidusBundle\Permission;

use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Permission;
use Sidus\SidusBundle\Entity\User;
use Sidus\SidusBundle\Entity\Version;
use Sidus\SidusBundle\Exception\AccessDeniedException;
use Symfony\Component\DependencyInjection\Container;

class PermissionManager {

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @var array
	 */
	protected $authCache = [];
	
	/**
	 * @var array
	 */
	protected $permissions = [];

	public function __construct(Container $container) {
		$this->container = $container;
	}

	/**
	 *
	 * @param Version $version
	 * @param int $permission
	 * @param string $method
	 * @return boolean
	 */
	public function hasPermission(Node $node, $permission, $method = null){
		$user = $this->getUser();
		if($user->hasRole('ROLE_ADMIN')){
			return true;
		}
		$mask = $this->getPermissionMaskForNode($node);
		return $mask->check($permission);
	}

	/**
	 *
	 * @param Node $node
	 * @return PermissionMask
	 */
	public function getPermissionMaskForNode(Node $node){
		if(isset($this->authCache[$node->getId()])){
			return $this->authCache[$node->getId()];
		}

		$entities = $this->getUserEntities();

		$compiled_mask = new PermissionMask;
		
		$permissions = $this->getPermissionsForEntities($node, $entities);
		$tmp_mask = $this->compilePermissions($permissions);
		if($tmp_mask->isOwner()){ // If owner of the current node, keeps the authorizations even if permissions are inherited
			$compiled_mask = $tmp_mask;
		}

		$ascendants = $node->getAscendants()->toArray();
		array_unshift($ascendants, $node); // add current node to array
		foreach($ascendants as $ascendant){
			$permissions = $this->getPermissionsForEntities($ascendant, $entities);
			$tmp_mask = $this->compilePermissions($permissions, $compiled_mask);
			if($tmp_mask->isAdmin()){ // If admin of any parent node, keeps the authorizations even if permissions are inherited
				$compiled_mask = $tmp_mask;
				break;
			}
			if($ascendant->getInheritPermissions()){
				continue;
			}
			$compiled_mask = $tmp_mask;
		}
		$this->authCache[$node->getId()] = $compiled_mask;
		return $compiled_mask;
	}

	/**
	 * 
	 * @param array $permissions
	 * @return \Sidus\SidusBundle\Permission\PermissionMask
	 */
	protected function compilePermissions(array $permissions, $mask = 0){
		if($mask instanceof PermissionMask){
			$mask = $mask->getMask();
		}
		foreach($permissions as $permission){
			$mask = $mask | $permission->getAuthorizations()->getMask();
		}
		foreach($permissions as $permission){
			$mask = $mask & ~$permission->getRestrictions()->getMask();
		}
		return new PermissionMask($mask);
	}

	/**
	 *
	 * @param string $persistentObjectName
	 * @param string $persistentManagerName
	 * @return ObjectRepository
	 */
	public function getRepository($persistentObjectName, $persistentManagerName = null) {
		return $this->getDoctrine()->getManager()->getRepository($persistentObjectName, $persistentManagerName);
	}

	/**
	 *
	 * @return User
	 */
	public function getUser(){
		$user_class = $this->container->getParameter('sidus.user.class');
		$token = $this->container->get('security.context')->getToken();
		$user = null;
		if($token){
			$user = $token->getUser();
		}
		if(null === $user || !is_a($user, $user_class)){
			$user = $this->getAnonymousUser();
		}
		return $user;
	}

	/**
	 *
	 * @return string
	 */
	public function getErrorMessage(){
		return $this->container->get('translator')->trans('Access Denied');
	}

	/**
	 *
	 * @param string $method
	 * @return int
	 */
	public function getPermissionMaskForMethod($method) {
		switch (substr($method, 0, 3)) {
			case 'get':
				return PermissionMask::READ;
			case 'set':
				return PermissionMask::WRITE;
			default:
				return PermissionMask::OWNER;
		}
	}

	/**
	 *
	 * @param Version $version
	 * @param int $mask
	 * @param string $method
	 * @throws AccessDeniedException
	 */
	public function enforcePermission(Node $node, $mask, $method = null){
		if(!$this->hasPermission($node, $mask, $method)){
			throw new AccessDeniedException("Trying to access following method: '{$method}' access denied by Permission Manager.");
		}
	}

	/**
	 * Warning, return value can be an other user class
	 * @return User
	 */
	public function getAnonymousUser(){
		$user_class = $this->container->getParameter('sidus.user.class');
		return new $user_class;
		$user_repo = $this->getDoctrine()->getManager()->getRepository($user_class);
		$anon = $user_repo->findOneBy(array('username' => 'anonymous'));
		if(null === $anon){
			$anon = new $user_class;
		}
		return $anon;
	}
	
	public function getPermissionsForNode(Node $node){
		return $this->getRepository('SidusBundle:Permission')->findByNode($node);
	}
	
	public function getPermissionsForEntities(Node $node, array $entities){
		return $this->getRepository('SidusBundle:Permission')
				->findByNodeAndEntities($node, $entities);
	}
	
	public function loadPermissionsForNodes($nodes){
		return $this->getRepository('SidusBundle:Permission')
				->findByNodesAndEntities($nodes, $this->getUserEntities(), false);
	}
	
	public function getUserEntities(){
		$user = $this->getUser();
		$entities = $user->getGroups();
		$entities[] = $user;
		return $entities;
	}

}
