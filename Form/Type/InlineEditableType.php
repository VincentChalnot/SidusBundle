<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InlineEditableType extends AbstractType {

	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array('tag' => 'div'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildView(FormView $view, FormInterface $form, array $options) {
		$view->vars['attr']['class'] = isset($options['attr']['class']) ? $options['attr']['class'].' html-editable' : 'html-editable';
		$view->vars['tag'] = $options['tag'];

	}

	/**
	 * {@inheritdoc}
	 */
	public function getParent() {
		return 'textarea';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName() {
		return 'inlineeditable';
	}

}
