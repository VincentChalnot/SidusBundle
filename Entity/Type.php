<?php

namespace Sidus\SidusBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\TypeRepository")
 * @UniqueEntity("typeName")
 * 
 */
class Type extends Object {

	/**
	 * @var string
	 * @ORM\Column(name="type_name", type="string", length=128, unique=true)
	 */
	private $typeName;

	/**
	 * @var string
	 * @ORM\Column(name="class_name", type="string", length=255)
	 */
	private $className;

	/**
	 * @var string
	 * @ORM\Column(name="bundle_name", type="string", length=128)
	 */
	private $bundleName;

	/**
	 * @var Type[]
	 * @ORM\ManyToMany(targetEntity="Sidus\SidusBundle\Entity\Type", cascade={"persist"})
	 * @ORM\JoinTable(name="authorized_type",
	 * 	joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
	 * 	inverseJoinColumns={@ORM\JoinColumn(name="authorized_type_id", referencedColumnName="id")}
	 * )
	 */
	private $authorizedTypes;

	/**
	 * @var Type[]
	 * @ORM\ManyToMany(targetEntity="Sidus\SidusBundle\Entity\Type", cascade={"persist"})
	 * @ORM\JoinTable(name="forbidden_type",
	 * 	joinColumns={@ORM\JoinColumn(name="type_id", referencedColumnName="id")},
	 * 	inverseJoinColumns={@ORM\JoinColumn(name="forbidden_type_id", referencedColumnName="id")}
	 * )
	 */
	private $forbiddenTypes;

	/**
	 *
	 * @var boolean
	 * @ORM\Column(name="is_versionned", type="boolean")
	 */
	private $isVersionned;

	/**
	 *
	 * @var boolean
	 * @ORM\Column(name="is_system", type="boolean")
	 */
	private $isSystem;

	public function __construct() {
		parent::__construct();
		$this->authorizedTypes = new ArrayCollection();
		$this->forbiddenTypes = new ArrayCollection();
		$this->isVersionned = true;
		$this->isSystem = false;
		$this->bundleName = 'SidusBundle';
	}

	/**
	 * Set typeName
	 *
	 * @param string $typeName
	 * @return Type
	 */
	public function setTypeName($typeName) {
		$this->typeName = $typeName;

		return $this;
	}

	/**
	 * Get typeName
	 *
	 * @return string 
	 */
	public function getTypeName() {
		return $this->typeName;
	}

	/**
	 * Set className
	 *
	 * @param string $className
	 * @return Type
	 */
	public function setClassName($className) {
		$this->className = $className;

		return $this;
	}

	/**
	 * Get className
	 *
	 * @return string 
	 */
	public function getClassName() {
		return $this->className;
	}

	/**
	 * Set bundleName
	 *
	 * @param string $bundleName
	 * @return Type
	 */
	public function setBundleName($bundleName) {
		$this->bundleName = $bundleName;

		return $this;
	}

	/**
	 * Get bundleName
	 *
	 * @return string 
	 */
	public function getBundleName() {
		return $this->bundleName;
	}

	/**
	 * Set isVersionned
	 *
	 * @param boolean $isVersionned
	 * @return Type
	 */
	public function setIsVersionned($isVersionned) {
		$this->isVersionned = $isVersionned;

		return $this;
	}

	/**
	 * Get isVersionned
	 *
	 * @return boolean 
	 */
	public function getIsVersionned() {
		return $this->isVersionned;
	}

	/**
	 * Set isSystem
	 *
	 * @param boolean $isSystem
	 * @return Type
	 */
	public function setIsSystem($isSystem) {
		$this->isSystem = $isSystem;

		return $this;
	}

	/**
	 * Get isSystem
	 *
	 * @return boolean 
	 */
	public function getIsSystem() {
		return $this->isSystem;
	}

	/**
	 * Add authorizedTypes
	 *
	 * @param \Sidus\SidusBundle\Entity\Type $authorizedTypes
	 * @return Type
	 */
	public function addAuthorizedType(\Sidus\SidusBundle\Entity\Type $authorizedTypes) {
		$this->authorizedTypes[] = $authorizedTypes;

		return $this;
	}

	/**
	 * Remove authorizedTypes
	 *
	 * @param \Sidus\SidusBundle\Entity\Type $authorizedTypes
	 */
	public function removeAuthorizedType(\Sidus\SidusBundle\Entity\Type $authorizedTypes) {
		$this->authorizedTypes->removeElement($authorizedTypes);
	}

	/**
	 * Get authorizedTypes
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getAuthorizedTypes() {
		return $this->authorizedTypes;
	}

	/**
	 * Add forbiddenTypes
	 *
	 * @param \Sidus\SidusBundle\Entity\Type $forbiddenTypes
	 * @return Type
	 */
	public function addForbiddenType(\Sidus\SidusBundle\Entity\Type $forbiddenTypes) {
		$this->forbiddenTypes[] = $forbiddenTypes;

		return $this;
	}

	/**
	 * Remove forbiddenTypes
	 *
	 * @param \Sidus\SidusBundle\Entity\Type $forbiddenTypes
	 */
	public function removeForbiddenType(\Sidus\SidusBundle\Entity\Type $forbiddenTypes) {
		$this->forbiddenTypes->removeElement($forbiddenTypes);
	}

	/**
	 * Get forbiddenTypes
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getForbiddenTypes() {
		return $this->forbiddenTypes;
	}

//	public function getFormType() {
//		return '\\Sidus\\SidusBundle\\Form\\Type\\TypeType';
//	}
}
