<?php

namespace Sidus\SidusBundle\OGM;

use Everyman\Neo4j\Client;
use Everyman\Neo4j\Index;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of NodeManager
 *
 * @author vincent
 */
class NodeManager {
	
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	protected $languages = [];

	/**
	 * @var Client
	 */
	protected $client;
	
	public function __construct(ContainerInterface $container, Client $client = null) {
		$this->container = $container;
		if($client){
			$this->client = $client;
		} else {
			$this->client = new Client();
		}
	}
	
	public function setLanguages() {
		
	}
	
	public function findOneByNodeName($nodeName) {
		$query = new \Everyman\Neo4j\Cypher\Query($this->client, 'START Test = node:SdNode(nodeName = {nodeName}) RETURN Test', ['nodeName' => $nodeName]);
		$rs = $this->client->executeCypherQuery($query);
		var_dump($rs);
	}
}
