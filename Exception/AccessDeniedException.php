<?php

namespace Sidus\SidusBundle\Exception;

/**
 * Description of AccessDeniedException
 *
 * @author vincent
 */
class AccessDeniedException extends \Exception {

	public function getShortMessage() {
		return 'Access denied';
	}

}
