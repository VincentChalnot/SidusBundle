<?php

namespace Sidus\SidusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table(name="folder")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\FolderRepository")
 */
class Folder extends Object {

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="tags", type="text", nullable=true)
	 */
	private $tags;

	public function __construct() {
		parent::__construct();
		$this->content = '';
		$this->tags = '';
	}

	/**
	 * @param string $content
	 * @return Page
	 */
	public function setContent($content) {
		$this->enforcePermision(__FUNCTION__);
		$this->content = (string)$content;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContent() {
		return $this->checkPermission(__FUNCTION__, $this->content);
	}

	/**
	 * @param string $tags
	 * @return Page
	 */
	public function setTags($tags) {
		$this->enforcePermision(__FUNCTION__);
		$this->tags = (string)$tags;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTags() {
		return $this->checkPermission(__FUNCTION__, $this->tags);
	}

}
