<?php

namespace Sidus\SidusBundle\Controller;

use DateTime;
use Sidus\SidusBundle\Action\DefaultAction;
use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Object;
use Sidus\SidusBundle\Entity\Type;
use Sidus\SidusBundle\Entity\User;
use Sidus\SidusBundle\Permission\PermissionManager;
use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Locale\Locale;

class CommonController extends Controller {

	protected $loadedObjects = array();
	protected $languages;

	/**
	 * @var Node
	 */
	protected $node;

	/**
	 * @var Object
	 */
	protected $object;

	/**
	 * All loaded nodes
	 *
	 * @var Node[]
	 */
	protected $nodes;

	/**
	 * @var array
	 */
	protected $translations;

	/**
	 * @var Node[]
	 */
	protected $ascendants;

	/**
	 * @var Node[]
	 */
	protected $descendants;

	/**
	 * @var Node[]
	 */
	protected $siblings;

	/**
	 * @var Node[]
	 */
	protected $children;

	/**
	 * @var Node[]
	 */
	protected $additional;

	/**
	 * @var string
	 */
	protected $locale = 'en';

	/**
	 * @var string
	 */
	protected $global_template = 'SidusBundle:Default:global.html.twig';

	/**
	 * @var DefaultAction
	 */
	protected $action;

	/**
	 * @var Response
	 */
	protected $response;

	/**
	 * Sets the Container associated with this Controller.
	 *
	 * @param ContainerInterface $container A ContainerInterface instance
	 */
	public function setContainer(ContainerInterface $container = null) {
		$this->container = $container;
		Object::setContainer($container);
	}

	/**
	 * Load all necessary objects related to a node given it's ID or it's name
	 * (node_name), it only loads the last version of the current node with the
	 * best language for $this->node. The other vers-
	 * ions for ascendants, descendants and siblings are also choosen automatic-
	 * ally so it might result in strange behavior if the language preferences
	 * on the client's browser are not set correctly.
	 *
	 * @param mixed $node_uid
	 * @param string $lang
	 */
	protected function loadObjectsForNodeUID($node_uid, $lang = null) {
		$this->setLanguage($lang);
		$this->action = $this->getAction();
		$this->format = $this->getRequestParameter('_format', 'html');
		$this->loadNodeByUID($node_uid);
		if (null === $this->node) {
			return;
		}
		$this->loadAscendants($this->node);
		if($this->action == 'show'){
			$this->loadChildren($this->node);
		}
		//$this->loadSiblings($this->node);
		$this->loadObjects($this->nodes);
		$this->loadObjectVersion();
		$this->translations = $this->getTranslations($this->object);
		$this->getPermissionManager()->loadPermissionsForNodes($this->nodes); // Load permissions cache
		$this->loadAdditional();
		$this->loadedObjects = array(
			'node' => $this->node,
			'translations' => $this->translations,
			'object' => $this->node->getObject(),
			'ascendants' => $this->ascendants,
			'children' => $this->children,
			'siblings' => $this->siblings,
			'locale' => $this->locale,
			'action' => $this->action,
			'global_template' => $this->global_template,
		);
		$this->loadedObjects['loadedObjects'] = $this->loadedObjects;
	}

	protected function loadObjectVersion() {
		$version = (int) $this->getRequestParameter('version');
		if ($version) {
			$object = $this->getNodeManager()->findByObjectVersion($version);
			if ($object && $object->getReference() === $this->node->getObjectReference()) {
				$this->node->setCurrentObject($object);
				$object->setCurrentNode($this->node);
				$this->setFlash('info', "You are reviewing the '{$object->getRevisionDate()->format('c')}' version of this object.");
			} else {
				$this->setFlash('error', "Unable to load version number {$version}, falling back to default version");
			}
		}
		$this->object = $this->node->getObject();
		return $this;
	}

	/**
	 * Load objects from an array of nodes, associate them together so you can call them with Node::getObject
	 * @param Node[] $nodes
	 */
	public function loadObjects(array $nodes) {
		$object_references = array();
		foreach ($nodes as $node) {
			$object_references[] = $node->getObjectReference();
		}
		$this->getNodeManager()->findSinglesByReferences($object_references);
		//The objects will be cached in the repository, no need to get them directly
		//We just load them afterward by calling
		foreach ($nodes as $node) {
			$object = $this->getNodeManager()->findOneByReference($node->getObjectReference());
			if ($object) {
				$node->setCurrentObject($object);
			} elseif ($this->getEnv() == 'dev') {
				var_dump($node, $object);
				xdebug_print_function_stack('Unable to retrieve object for node');
				exit;
			}
		}
		return $this;
	}

	/**
	 * Loads the node related by the given node UID.
	 *
	 * @param mixed $node_uid
	 * @return Node
	 */
	protected function loadNodeByUID($node_uid) {
		if (is_numeric($node_uid)) {
			$this->node = $this->getNodeManager()->find($node_uid);
		} else {
			$this->node = $this->getNodeManager()->findOneByNodeName($node_uid);
		}
		$this->registerNode($this->node);
		return $this;
	}

	protected function loadAscendants(Node $node) {
		$this->ascendants = $this->getNodeManager()->getPath($node);
		array_pop($this->ascendants);
		$this->registerNodes($this->ascendants);
		return $this;
	}

	protected function loadDescendants(Node $node) {
		$this->descendants = $this->getNodeManager()->childrenQueryBuilder($node)->getQuery()->getResult();
		$this->registerNodes($this->descendants);
		return $this;
	}

	/**
	 * @param Node $node
	 * @return Node[]
	 */
	protected function loadChildren(Node $node) {
		$paginator = $this->get('knp_paginator');
		$paginator->reset()
				->setPage($this->getRequestParameter('page', 1))
				->setLimit(10);

		$this->children = $paginator->paginate($this->getNodeManager()->childrenQueryBuilder($node, true));
		$this->registerNodes($this->children);
		return $this;
	}

	protected function registerNode(Node $node = null) {
		if ($node) {
			$this->nodes[$node->getId()] = $node;
		}
	}

	protected function registerNodes($nodes) {
		foreach ($nodes as $node) {
			if ($node instanceof Node) {
				$this->nodes[$node->getId()] = $node;
			} elseif ($this->getEnv() == 'dev') {
				var_dump($node);
				xdebug_print_function_stack('Unable to register node');
				exit;
			}
		}
	}

	/**
	 * @param Node $node
	 * @return Node[]
	 */
	protected function loadSiblings(Node $node) {
		$paginator = $this->get('knp_paginator');
		$paginator->reset()
				->setPage($this->getRequestParameter('siblings_page', 1))
				->setLimit(10)
				->setOption('sortFieldParameterName', 'siblings_sort')
				->setOption('pageParameterName', 'siblings_page');

		if (null === $node->getParent()) {
			$this->siblings = $paginator->paginate($this->getNodeManager()->getRootNodesQueryBuilder());
		} else {
			$this->siblings = $paginator->paginate($this->getNodeManager()->childrenQueryBuilder($node->getParent(), true));
		}
		$this->registerNodes($this->siblings);
		return $this;
	}

	/**
	 * Load objects for additional nodes (RevisionBy for each node)
	 */
	protected function loadAdditional() {
		$additional_nodes = array();
		$additional_references = array();
		foreach ($this->nodes as $node) {
			if (null === $node->getObject()) {
				continue;
			}
			if (!$node->getObject()->hasPermission()) {
				continue;
			}
			$rb = $node->getObject()->getRevisionBy();
			if ($rb) {
				$additional_nodes[] = $rb;
				$additional_references[] = $rb->getObjectReference();
				$this->registerNode($rb);
			}
		}

		$this->getNodeManager()->findByReferences($additional_references);
		//The objects will be cached in the repository, no need to get them directly
		//We just load them afterward by calling
		$this->getPermissionManager()->loadPermissionsForNodes($additional_nodes); // Load Permission cache
		foreach ($additional_nodes as $node) {
			$node->setCurrentObject($this->getNodeManager()->findOneByReference($node->getObjectReference()));
		}
		return $this;
	}

	/**
	 * Get the current session or create a new one
	 *
	 * @return Session $session
	 */
	public function getSession() {
		$session = $this->get('session');
		if (!$session) {
			$session = new Session;
			$session->start();
			$session->set('lang', $this->getRequest()->getLocale());
		}
		return $session;
	}

	public function setFlash($name, $value) {
		$this->getSession()->getFlashBag()->add($name, $value);
		return $this;
	}

	public function getRequestParameter($path, $default = null, $deep = false) {
		return $this->getRequest()->get($path, $default, $deep);
	}

	public function setLanguage($prefered_lang = null) {
		if (null === $prefered_lang) {
			$prefered_lang = $this->getSession()->get('_locale');
		}
		$languages = $this->getRequest()->getLanguages();
		foreach ($languages as &$lang) {
			$lang = substr($lang, 0, 2);
		}
		if (null !== $prefered_lang) {
			array_unshift($languages, $prefered_lang);
		}
		array_push($languages, Locale::getDefault());
		$languages = array_values(array_unique($languages));
		$this->languages = $languages;
		$this->getNodeManager()->setLanguages($languages);
		$this->locale = $this->getPreferedLanguage();
		$this->getSession()->set('_locale', $this->locale);
		$this->get('translator')->setLocale($this->locale);
		return $this;
	}

	/**
	 * Get all languages
	 *
	 * @return array
	 */
	public function getLanguages() {
		if (null === $this->languages) {
			$this->setLanguage($this->getRequestParameter('lang'));
		}
		return $this->languages;
	}

	/**
	 * Return prefered language
	 *
	 * @return string
	 */
	public function getPreferedLanguage() {
		$languages = $this->getLanguages();
		return array_shift($languages);
	}

	/**
	 * @TODO : CommonController::getUser() must return instance of Node instead of User
	 * Well, not so sure...
	 * @return User
	 */
	public function getUser() {
		return $this->get('security.context')->getToken()->getUser();
		$pmanager = $this->get('sidus.permission.manager');
		return $pmanager->getAnonymousUser();
	}

	public function getUserNode() {
		return $this->getNodeManager()->find($this->getUser()->getReference());
	}

	/**
	 * 
	 * @return boolean
	 */
	public function getIsUserLoggedIn() {
		return $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED');
	}

	/**
	 * 
	 * @return DefaultAction
	 */
	protected function getAction($name = null) {
		if (null === $name) {
			$name = $this->getRequestParameter('action', 'show');
		}
		return $this->get('sidus.action.manager')->getAction($name);
	}

	protected function isValidController($controller) {
		$parser = new ControllerNameParser($this->get('kernel'));
		list($class, $method) = explode('::', $parser->parse($controller));
		return is_callable(array($class, $method));
	}

	public function getPathForNode($node, $action = 'show', array $options = []) {
		return $this->get('twig.extension.sidus_node')->getPathForNode($node, $action, $options);
	}

	protected function getEnv() {
		return $this->container->getParameter('kernel.environment');
	}

	/**
	 * 
	 * @return Response
	 */
	protected function getResponse() {
		if (!$this->response) {
			$this->response = new Response;
		}
		return $this->response;
	}

	protected function getTranslations(Object $object) {
		return $this->getNodeManager()->getTranslationsForObject($object);
	}


	/**
	 * 
	 * @param Object $object
	 * @return Form
	 */
	protected function buildForm(Object $object) {
		$builder = $this->createFormBuilder($object);
		$builder->add('title', 'text');
		$em = $this->getNodeManager();
		foreach ($em->getClassMetadata(get_class($object))->fieldMappings as $mapping) {
			if (in_array($mapping['fieldName'], $object::getReservedProperties())) {
				continue;
			}
//			if($mapping['fieldName'] == 'content'){ // Maybe not a good idea after all...
//				$builder->add('content', 'richtextarea', ['attr' => array('class' => 'content'), 'required' => false]);
//				continue;
//			}
			if(!$object->checkPermission('set'.$mapping['fieldName'], PermissionMask::WRITE)){
				continue;
			}
			$options = ['required' => !$mapping['nullable']];
			switch ($mapping['type']) {
				case 'integer':
				case 'smallInt':
				case 'bigInt':
					$type = 'integer';
					break;
				case 'string':
					$type = 'text';
					break;
				case 'text':
					$type = 'textarea';
					break;
				case 'float':
				case 'decimal':
					$type = 'number';
					break;
				case 'boolean':
					$type = 'choice';
					$options['choices'] = [ 1 => 'yes', 0 => 'no'];
					break;
				case 'datetime':
					$type = 'datetime';
					break;
				case 'date':
					$type = 'date';
					break;
				case 'time':
					$type = 'time';
					break;
				default:
					$this->setFlash('warning', "Field '{$mapping['fieldName']}' of type '{$mapping['type']}' not mapped automatically");
					continue;
			}
			$builder->add($mapping['fieldName'], $type, $options);
		}
		return $builder->getForm();
	}
	
	protected function getFormForObject(Object $object){
		$formTypeClass = $object->getFormType();
		if ($formTypeClass && class_exists($formTypeClass)) {
			return $this->createForm(new $formTypeClass, $object);
		}
		return $this->buildForm($object);
	}

	/**
	 * Get the twig template name for a bundle and an action.
	 * @param string $bundle
	 * @param string $action
	 * @param string $format
	 * @return string
	 */
	protected function getTemplateName($bundle, $typeName, $action, $format = 'html') {
		$template = "{$bundle}:{$typeName}:{$action}.{$format}.twig";
		$message = null;
		if (!$this->get('templating')->exists($template) && $bundle != 'SidusBundle') {
			if ($this->getEnv() == 'dev') {
				//$message = "[DEV] Could not find template '{$template}', falling back to Sidus's template for action {$action}";
			}
			$template = "SiduBundle:{$typeName}:{$action}.{$format}.twig";
		}
		if (!$this->get('templating')->exists($template)) {
			if ($this->getEnv() == 'dev') {
				//$message = "[DEV] Could not find template '{$template}', falling back to Bundle default template for action {$action}";
			}
			$template = "{$bundle}:Default:{$action}.{$format}.twig";
		}
		if (!$this->get('templating')->exists($template) && $bundle != 'SidusBundle') {
			if ($this->getEnv() == 'dev') {
				//$message = "[DEV] Could not find template '{$template}', falling back to Sidus's default template for action {$action}";
			}
			$template = "SidusBundle:Default:{$action}.{$format}.twig";
		}
		if (!$this->get('templating')->exists($template)) {
			$template = "SidusBundle:Default:show.{$format}.twig";
			$this->setFlash('error', "Missing view for '{$action}' action");
		} elseif ($message) {
			$this->setFlash('warning', $message);
		}
		return $template;
	}
	
	/**
	 * This action is called when a node is not found
	 * @return Response
	 */
	public function notFoundAction() {
		//@TODO traduction
		$params['status_text'] = 'Sorry, we were unable to find the requested content.';
		$params['locale'] = $this->getPreferedLanguage();
		$params['global_template'] = $this->global_template;
		return $this->render("SidusBundle:Exception:error404.html.twig", $params)->setStatusCode(404);
	}

	/**
	 * This action is called when a node is not found
	 * @return Response
	 */
	public function unauthorizedAction($loadedObjects) {
		//@TODO traduction
		$loadedObjects['status_text'] = 'You do not have the credentials to access this content.';
		return $this->render("SidusBundle:Exception:error401.html.twig", $loadedObjects)->setStatusCode(401);
	}
	
	/**
	 * @todo needs specifications !
	 * @param Object $object
	 * @return Type
	 */
	protected function loadTypes(Object $object) {
		$nm = $this->getNodeManager();

		$forbidden_types = $object->getType()->getForbiddenTypes();
		$authorized_types = $object->getType()->getAuthorizedTypes();

		if ($forbidden_types->isEmpty() && $authorized_types->isEmpty()) {
			$types = new ArrayCollection($nm->findAllTypes());
		}

		if (!$forbidden_types->isEmpty() && $authorized_types->isEmpty()) {
			$types = new ArrayCollection($nm->findAllTypes());
			foreach ($forbidden_types->toArray() as $type) {
				$types->removeElement($type);
			}
		}
		if (!$authorized_types->isEmpty() && $forbidden_types->isEmpty()) {
			$types = $authorized_types;
		}
		return $types;
	}
	
	protected function getAvailableLanguages(){
		return Intl::getLanguageBundle()->getLanguageNames();
	}
	
	/**
	 * 
	 * @return PermissionManager
	 */
	protected function getPermissionManager(){
		return $this->get('sidus.permission.manager');
	}
	
	protected function load(array $loadedObjects){
		foreach($loadedObjects as $key => $value){
			if(property_exists($this, $key)){
				$this->$key = $value;
			}
		}
	}
	
	protected function continueWithDefault($loadedObjects){
		$response = new Response;
		$response->loadedObjects = $loadedObjects;
		return $response;
	}
	
	/**
	 * Refresh last login date if user just logged in
	 * @return null
	 */
	protected function refreshUser(){
		$token = $this->get('security.context')->getToken();
		if(!$token->hasAttribute('hasJustLoggedIn') || !($this->getUser() instanceof User)){
			return;
		}
		$user = $this->getUser();
		if(!$token->getAttribute('hasJustLoggedIn') || !$user->hasPermission(PermissionMask::OWNER)){
			return;
		}
		$user->setLastLogin(new DateTime);
		$em = $this->getNodeManager();
		$em->persist($user);
		$em->flush();
		$token->setAttribute('hasJustLoggedIn', false);
	}
	
	protected function getNodeManager() {
		return $this->get('sidus.node.manager');
	}
}
