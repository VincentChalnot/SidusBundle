
$(document).ready(function() {
	// Activate tooltip for node actions
	$('.node-actions a').tooltip({placement: 'right'});
	
	// Initialize TinyMCE
	tinyMCE.baseURL = '/bundles/sidus/js/tinymce';
	tinyMCE.init({
		plugins: 'link code textcolor autolink',
		selector:'textarea.rich_textarea',
		menubar: false,
		browser_spellcheck : true,
		toolbar: "bold italic outdent indent bullist numlist forecolor | link unlink | styleselect fontselect fontsizeselect | removeformat code",
		extended_valid_elements: 'pre[class|style|title]'
	});
	tinyMCE.init({
		selector:'.text-editable',
		inline: true,
		menubar: false,
		statusbar : false,
		fixed_toolbar_container: "#tinymce_toolbar",
		toolbar: "undo redo"
	});
	tinyMCE.init({
		plugins: 'link code textcolor autolink',
		selector:'.html-editable',
		inline: true,
		menubar: false,
		browser_spellcheck : true,
		statusbar : false,
		fixed_toolbar_container: "#tinymce_toolbar",
		toolbar: "bold italic outdent indent bullist numlist blockquote forecolor | link unlink | styleselect fontselect fontsizeselect | removeformat code",
		extended_valid_elements: 'pre[class|style|title]'
	});
});
