<?php

namespace Sidus\SidusBundle\Handler;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {

	/**
	 * @var Router
	 */
	protected $router;

	public function __construct(Router $router) {
		$this->router = $router;
	}

	public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
		$request->getSession()->getFlashBag()->add('success', "Login successful, welcome {$token->getUsername()}.");
		$token->setAttribute('hasJustLoggedIn', true);
		return new RedirectResponse($this->router->generate('sidus_show_node', array('node_id' => $request->get('node_id'))));
	}

}