<?php

namespace Sidus\SidusBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class UserController extends CommonController {

	public function loginAction($loadedObjects, Request $request) {
		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
		} else {
			$error = $this->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
			$this->getSession()->remove(SecurityContext::AUTHENTICATION_ERROR);
		}

		$loadedObjects['last_username'] = $this->getSession()->get(SecurityContext::LAST_USERNAME);
		$response = $this->render('SidusBundle:User:login.html.twig', $loadedObjects);
		if (null !== $error) {
			$this->getSession()->getFlashBag()->add('error', $error->getMessage());
			$response->setStatusCode(401, 'Login failed');
		}
		return $response;
	}

}
