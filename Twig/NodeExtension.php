<?php

namespace Sidus\SidusBundle\Twig;

use DateTime;
use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Object;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Twig_Extension;
use Twig_Function_Method;
use UnexpectedValueException;

class NodeExtension extends Twig_Extension {

	/**
	 * @var NodeManager
	 */
	protected $nodeManager;

	/**
	 * @var RouterInterface 
	 */
	protected $router;

	/**
	 * @var Request
	 */
	protected $container;

	public function __construct(ContainerInterface $serviceContainer) {
		$this->nodeManager = $serviceContainer->get('sidus.node.manager');
		$this->router = $serviceContainer->get('router');
		$this->container = $serviceContainer;
	}

	public function getName() {
		return 'sidus_node';
	}

	public function getFunctions() {
		return array(
			'path_to_node' => new Twig_Function_Method($this, 'getPathForNode'),
		);
	}

	protected function getNodeManager() {
		return $this->nodeManager;
	}

	public function getPathForNode($node, $action = 'show', array $options = []) {
		if ($node instanceof Node) {
			$options['node_id'] = $node->getId();
		} elseif ($node instanceof Object) {
			$node = $this->getNodeManager()->findOneByObject($node);
			if ($node) {
				$options['node_id'] = $node->getId();
			} else {
				throw new UnexpectedValueException('Node not found');
			}
		} elseif (method_exists($node, 'getNodeId')) {
			$options['node_id'] = $node->getNodeId();
		} elseif (method_exists($node, 'getNode') && $node->getNode()) {
			$options['node_id'] = $node->getNode()->getId();
		} else {
			throw new UnexpectedValueException('Unable to parse given object');
		}
		$options['action'] = $action;
		if (isset($options['version']) && $options['version'] instanceof DateTime) {
			$options['version'] = $options['version']->format('U');
		}
		if ($action == 'feed') {
			return $this->router->generate('sidus_node_feed', $options);
		}
		return $this->router->generate('sidus_node_action', $options);
	}

}