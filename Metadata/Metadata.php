<?php

namespace Sidus\SidusBundle\Metadata;

use DateTime;
use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Object;

class Metadata {

	protected $name;
	protected $type;
	protected $object;
	protected $value;
	protected $renderer;
	protected $label;

	public function __construct($name, $object, $value, $type = null) {
		$this->name = $name;
		$this->object = $object;
		$this->value = $value;
		if ($type) {
			$this->type = $type;
		} elseif($value instanceof Node || $value instanceof Object) {
			$this->type = 'node';
		} else {
			$this->type = gettype($value);
		}
	}

	public function getName() {
		return $this->name;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	public function getObject() {
		return $this->object;
	}

	public function getValue() {
		return $this->value;
	}

	public function getRenderer() {
		return $this->renderer;
	}

	public function setRenderer($renderer) {
		$this->renderer = $renderer;
		return $this;
	}

	public function getLabel() {
		if ($this->label !== null) {
			return $this->label;
		}
		return $this->humanize($this->getName());
	}

	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	public function __toString() {
		$v = $this->getValue();
		switch ($this->getType()) {
			case 'string':
				return (string) $v;
			case 'boolean':
				return $v ? 'Yes' : 'No';
			case 'double':
			case 'integer':
				return (string) $v;
			case 'object':
				if (method_exists($v, '__toString')) {
					return (string) $v;
				}
				if($v instanceof DateTime){
					return (string)$v->format('Y-m-d H:i:s');
				}
		}
		if($v == null){
			return '';
		}
		return 'Not displayable';
	}

	/**
	 * 
	 * @param string $text
	 * @return string
	 */
	public function humanize($text) {
		return ucfirst(trim(strtolower(preg_replace(array('/([A-Z])/', '/[_\s]+/'), array('_$1', ' '), $text))));
	}

}