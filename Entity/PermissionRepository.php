<?php

namespace Sidus\SidusBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PermissionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PermissionRepository extends EntityRepository {

	protected $cache = [];
	
	/**
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $node
	 * @param Object[] $entities
	 */
	public function findByNodeAndEntities(Node $node, array $entities){
		if($this->hasCachedPermission($node)){
			$result = [];
			foreach($entities as $entity){
				if($this->hasCachedPermission($node, $entity)){
					$permission = $this->getCachedPermission($node, $entity);
					if($permission) {
						$result[] = $permission;
					}
				}
			}
			return $result;
		}
		return $this->findByNodesAndEntities([$node], $entities);
	}
	
	/**
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $node
	 * @param Object[] $entities
	 */
	public function findByNodesAndEntities(array $nodes, array $entities, $useCache = true){
		$result = [];
		// @TODO get results from cache
		$qb = $this->createQueryBuilder('p')
				->select('p')
				->where('p.node IN (:nodes)')
				->andWhere('p.entity IN (:entities)')
				->setParameter('nodes', $nodes)
				->setParameter('entities', $entities);
		$more = $this->cacheResult($qb->getQuery()->getResult(), $nodes, $entities);
		return array_merge($result, $more);
	}
	
	public function clearCache() {
		$this->cache = array();
		return $this;
	}

	/**
	 * 
	 * @param mixed $result an array of objects or a object
	 * @return Object[]
	 */
	protected function cacheResult($result, $nodes = null, $entities = null) {
		if (is_array($result)) {
			foreach ($result as $sub_result) {
				if($sub_result instanceof Permission){
					$this->cachePermission($sub_result);
				}
			}
		} elseif ($result instanceof Permission) {
			$this->cachePermission($result);
		}
		if(null !== $nodes && null !== $entities){
			foreach($nodes as $node){
				foreach($entities as $entity){
					if(!$this->hasCachedPermission($node, $entity)){
						$this->cache[$node->getId()][$entity->getId()] = null;
					}
				}
			}
		}
		return $result;
	}
	
	protected function cachePermission(Permission $permission){
		$this->cache[$permission->getNode()->getId()][$permission->getEntity()->getId()] = $permission;
		return $permission;
	}

	/**
	 * @param int $reference
	 * @param string $lang
	 * @return boolean
	 */
	protected function hasCachedPermission($nodeId, $entityId = null) {
		if($nodeId instanceof Node){
			$nodeId = $nodeId->getId();
		}
		if($entityId instanceof Object){
			$entityId = $entityId->getId();
		}
		if($entityId === null){
			return isset($this->cache[$nodeId]);
		}
		return isset($this->cache[$nodeId][$entityId]);
	}

	/**
	 * @param int $reference
	 * @param string $lang
	 * @return Object
	 */
	protected function getCachedPermission($nodeId, $entityId = null) {
		if($nodeId instanceof Node){
			$nodeId = $nodeId->getId();
		}
		if($entityId instanceof Object){
			$entityId = $entityId->getId();
		}
		if($entityId && isset($this->cache[$nodeId][$entityId])){
			return $this->cache[$nodeId][$entityId];
		}
		if(isset($this->cache[$nodeId])){
			return $this->cache[$nodeId];
		}
		throw new \InvalidArgumentException("Incorrect nodeId {$nodeId}");
	}
	
	public function getCachedPermissions(){
		$permissions = [];
		foreach ($this->cache as $tmp){
			foreach($tmp as $permission){
				$permissions[$permission->getId()] = $permission;
			}
		}
		return $permissions;
	}

}
