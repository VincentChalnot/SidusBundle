SidusBundle
===========

Sidus is an open source Node-oriented framework based on Symfony2
It's currently a work in progress but hey, it's already working !

It allows you to concentrate on your model while it does:

 - Manage advanced Access Control Lists for your objects with heritage, and there is nothing to configure in the code, it's all in the user-friendly backend !
 - Type your objects independently of your model, and skin each type with specific views, or not, Sidus can check automatically if your templates exists or if it needs to fallback to a default view.
 - The backend is the frontend, there is no ugly CRUD layout lying in the way, it's all smoothly integrated with Twitter's Bootstrap and TinyMCE for rich text edition and inline-editing.
 - Never lose any critical informations, all objects are automatically versionned ! It also garantee that there will be no problems with concurrent edition of the same object.
 - Oh, and did I told you ? It's i18n friendly: you can translate any object and the system will automatically switch to the language you choose if it's available.

Fork the [SidusAdmin](https://github.com/VincentChalnot/SidusAdmin) repo to start a new application.

## In a nutshell

In Sidus, there are objects and nodes and each node is typed.
In a standard application, each node has many objects linked to it identified by a common reference, each of them identified by versions.
For more complex applications, Sidus allows you to link several nodes to the same group of objects, thus allowing you to multi-parent a node.

There are two types of versions: The first one is identified by a revision number to record every different versions the object has gone through.
The second type of versions is the language, which allows you to keep a translated copy of an object.

Nodes are typed, meaning that a node can be a Page, a Folder, a User, and even a Type (yes, Types are also nodes) or basically anything you need.
The type is linked to a Doctrine Entity (the object) and to a controller. The typeName is used to select a proper view to render the object.

Now the good news, once you have written your Doctrine Entity that extends SidusBundle:Object, you just need to create a new Type... And that's it !
You actually don't need to write anything in your controller, you don't need to write any code for the view and you'll already have a (basic) working application.
