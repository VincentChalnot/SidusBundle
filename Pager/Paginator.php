<?php

namespace Sidus\SidusBundle\Pager;

use Knp\Component\Pager\Paginator as KnpPaginator;

class Paginator extends KnpPaginator {

	protected $page = 1;
	protected $limit = 10;
	protected $options = array();

	public function paginate($target, $page = null, $limit = null, $options = null) {
		if (null === $page) {
			$page = $this->getPage();
		}
		if (null === $limit) {
			$limit = $this->getLimit();
		}
		if (null === $options) {
			$options = $this->getOptions();
		}
		return parent::paginate($target, $page, $limit, $options);
	}

	public function getPage() {
		return $this->page;
	}

	public function setPage($page) {
		$this->page = (int) $page;
		return $this;
	}

	public function getLimit() {
		return $this->limit;
	}

	public function setLimit($limit) {
		$this->limit = (int) $limit;
		return $this;
	}

	public function getOptions() {
		return $this->options;
	}

	public function setOptions(array $options) {
		$this->options = $options;
		return $this;
	}

	public function getOption($key) {
		if (isset($this->options[$key])) {
			return $this->options[$key];
		}
	}

	public function setOption($key, $value = null) {
		$this->options[$key] = $value;
		return $this;
	}

	public function reset(){
		$this->page = 1;
		$this->limit = 10;
		$this->options = array();
		return $this;
	}

}
