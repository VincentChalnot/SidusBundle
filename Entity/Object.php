<?php

namespace Sidus\SidusBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Sidus\SidusBundle\Permission\PermissionManager;
use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Object
 *
 * @ORM\Table(name="object")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\ObjectRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="class", type="string", length=64)
 * @ORM\DiscriminatorMap({
  "SidusBundle:Object": "Sidus\SidusBundle\Entity\Object",
  "SidusBundle:Type": "Sidus\SidusBundle\Entity\Type",
  "SidusBundle:Folder": "Sidus\SidusBundle\Entity\Folder",
  "SidusBundle:Page": "Sidus\SidusBundle\Entity\Page",
  "SidusBundle:User": "Sidus\SidusBundle\Entity\User",
  })
 * The DiscriminatorMap is automatically loaded from the Types
 * See DiscriminatorListener for the magic
 */
class Object {

	/**
	 * @var integer
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="title", type="string", length=255)
	 * @Assert\Length(max=255)
	 * @Assert\NotBlank()
	 */
	private $title;

	/**
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Type")
	 * @ORM\JoinColumn(name="type_id")
	 */
	private $type;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="reference", type="integer")
	 * @Assert\NotBlank(groups={"add"})
	 */
	private $reference;

	/**
	 * @var string
	 * @ORM\Column(name="lang", type="string", length=3)
	 */
	private $lang;

	/**
	 * @var DateTime
	 *
	 * @ORM\Column(name="creation_date", type="datetime")
	 * @Assert\NotBlank(groups={"add"})
	 * @Assert\DateTime()
	 */
	private $creationDate;

	/**
	 * @var DateTime
	 *
	 * @ORM\Column(name="revision_date", type="datetime")
	 * @Assert\NotBlank(groups={"add"})
	 * @Assert\DateTime()
	 */
	private $revisionDate;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_head", type="boolean")
	 */
	private $isHead;

	/**
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Node")
	 * @ORM\JoinColumn(name="revision_by_id")
	 * @Assert\NotBlank(groups={"add"})
	 */
	private $revisionBy;

	/**
	 * @var Permission[]
	 * @ORM\OneToMany(targetEntity="Sidus\SidusBundle\Entity\Permission", mappedBy="entity", cascade={"refresh","remove"})
	 */
	private $authorizations;

	/**
	 * Node by which the system loaded the object
	 * @var Node
	 */
	private $currentNode;

	/**
	 *
	 * @var PermissionManager
	 */
	private $permissionManager;

	/**
	 *
	 * @var PermissionManager
	 */
	private $actionManager;

	/**
	 * @var ContainerInterface
	 *
	 * @api
	 */
	protected static $container;
	
	protected static $reservedProperties =  ['id', 'type', 'reference', 'lang', 'creationDate', 'reservedProperties',
		'revisionDate', 'isHead', 'currentNode', 'revisionBy', 'authorizations', 'metadata', 'formType', 'actions' ];

	/**
	 * Sets the Container associated with this Controller.
	 *
	 * @param ContainerInterface $container A ContainerInterface instance
	 *
	 * @api
	 */
	public static function setContainer(ContainerInterface $container = null) {
		self::$container = $container;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->creationDate = new DateTime;
		$this->revisionDate = new DateTime;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	protected function setId($id) {
		$this->id = $id;
	}

	/**
	 * Set type
	 *
	 * @param Type $type
	 * @return Node
	 */
	public function setType($type) {
		$this->enforcePermision(__FUNCTION__);
		$this->type = $type;
		return $this;
	}

	/**
	 * Get type
	 *
	 * @return Type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Object
	 */
	public function setTitle($title) {
		$this->enforcePermision(__FUNCTION__);
		$this->title = $title;
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->checkPermission(__FUNCTION__, $this->title);
	}

	/**
	 * This is very important that this function is not extended in subclasses,
	 * it is used to order list of object by name.
	 * @return string
	 */
	final public function __toString() {
		try {
			return (string) $this->getTitle();
		} catch (Exception $e) {
			return 'Error';
		}
	}

	/**
	 * Returns the node that loaded the object
	 * @return Node
	 */
	public function getCurrentNode() {
		if (!$this->currentNode && $this->reference) {
			$node = self::$container->get('doctrine')
							->getRepository('SidusBundle:Node')
							->findOneByObjectReference($this->reference);
			if($node){
				$this->setCurrentNode($node);
			} else {
				var_dump($this->reference, $node);
			}
		}
		return $this->currentNode;
	}

	/**
	 * Check if object is already associated with a node
	 * @return bool
	 */
	public function hasCurrentNode() {
		return (bool) $this->currentNode;
	}

	/**
	 * Set current version during the loading of the object
	 * @param \Sidus\SidusBundle\Entity\Node $currentNode
	 * @return \Sidus\SidusBundle\Entity\Object
	 */
	public function setCurrentNode(Node $currentNode) {
		$this->currentNode = $currentNode;
		return $this;
	}

	/**
	 * 
	 * @return PermissionManager
	 */
	protected function getPermissionManager() {
		if (null === $this->permissionManager) {
			$this->permissionManager = self::$container->get('sidus.permission.manager');
		}
		return $this->permissionManager;
	}

	/**
	 *
	 * @param string $method
	 * @param int $mask
	 * @return boolean
	 */
	public function hasPermission($mask = PermissionMask::READ, $method = null) {
		if (null === $this->getCurrentNode() || null === $this->getPermissionManager()) {
			return true;
		}
		return $this->getPermissionManager()->hasPermission($this->getCurrentNode(), $mask, $method = null);
	}

	/**
	 *
	 * @param string $method
	 * @param mixed $value
	 * @param int $mask
	 * @return mixed The value or the error message
	 */
	public function checkPermission($method, $value, $mask = null) {
		if (null === $this->getPermissionManager()) {
			return $value;
		}
		if (null === $mask) {
			$mask = $this->getPermissionManager()->getPermissionMaskForMethod($method);
		}
		if ($this->hasPermission($mask, $method)) {
			return $value;
		}
		return $this->getPermissionManager()->getErrorMessage();
	}

	/**
	 *
	 * @param string $method
	 * @param int $mask
	 * @return null
	 */
	public function enforcePermision($method, $mask = null) {
		if (null === $this->getCurrentNode() || null === $this->getPermissionManager()) {
			return;
		}
		if (null === $mask) {
			$mask = $this->getPermissionManager()->getPermissionMaskForMethod($method);
		}
		$this->getPermissionManager()->enforcePermission($this->getCurrentNode(), $mask, $method);
	}

	/**
	 * Set reference
	 *
	 * @param integer $reference
	 * @return Object
	 */
	public function setReference($reference) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::OWNER);
		$this->reference = $reference;
		return $this;
	}

	/**
	 * Get reference
	 *
	 * @return integer
	 */
	public function getReference() {
		return $this->reference;
	}

	/**
	 * Set lang
	 *
	 * @param string $lang
	 * @return Object
	 */
	public function setLang($lang) {
		$this->enforcePermision(__FUNCTION__);
		$this->lang = $lang;
		return $this;
	}

	/**
	 * Get lang
	 *
	 * @return string
	 */
	public function getLang() {
		return $this->lang;
	}

	/**
	 * Set creationDate
	 *
	 * @param DateTime $creationDate
	 * @return Object
	 */
	public function setCreationDate($creationDate) {
		$this->enforcePermision(__FUNCTION__, PermissionMask::ADMIN);
		$this->creationDate = $creationDate;
		return $this;
	}

	/**
	 * Get creationDate
	 *
	 * @return DateTime
	 */
	public function getCreationDate() {
		return $this->checkPermission(__FUNCTION__, $this->creationDate);
	}

	/**
	 * Set revisionDate
	 *
	 * @param DateTime $revisionDate
	 * @return Object
	 */
	public function setRevisionDate($revisionDate) {
		$this->enforcePermision(__FUNCTION__);
		$this->revisionDate = $revisionDate;
		return $this;
	}

	/**
	 * Get revisionDate
	 *
	 * @return DateTime
	 */
	public function getRevisionDate() {
		return $this->checkPermission(__FUNCTION__, $this->revisionDate);
	}

	/**
	 * Set revisionBy
	 *
	 * @param \Sidus\SidusBundle\Entity\Node $revisionBy
	 * @return Object
	 */
	public function setRevisionBy(\Sidus\SidusBundle\Entity\Node $revisionBy = null) {
		$this->enforcePermision(__FUNCTION__);
		$this->revisionBy = $revisionBy;
		return $this;
	}

	/**
	 * Get revisionBy
	 *
	 * @return \Sidus\SidusBundle\Entity\Node
	 */
	public function getRevisionBy() {
		return $this->checkPermission(__FUNCTION__, $this->revisionBy);
	}

	protected function getActionManager() {
		if (null === $this->actionManager) {
			$this->actionManager = self::$container->get('sidus.action.manager');
		}
		return $this->actionManager;
	}

	/**
	 * 
	 * @return \Sidus\SidusBundle\Action\ActionInterface[]
	 */
	public function getActions() {
		$actions = $this->getActionManager()->getActionsForObject($this);
		if(!$this->getType()->getIsVersionned()){
			unset($actions['history']);
			unset($actions['translate']);
		}
		if($this->getType()->getIsSystem()){
			foreach($actions as $action){
				if($action->getName() != 'show'){
					$action->setPermissionMask(new PermissionMask('admin'));
				}
			}
		}
		return $actions;
	}

	public function getFormType() {
		return '\\Sidus\\SidusBundle\\Form\\Type\\ObjectType';
	}

	/**
	 * Set isHead
	 * @param boolean $isHead
	 * @return Object
	 */
	public function setIsHead($isHead) {
		$this->isHead = (bool) $isHead;
		return $this;
	}

	/**
	 * Get isHead
	 * @return boolean 
	 */
	public function getIsHead() {
		return $this->isHead;
	}

	/**
	 * Add authorizations
	 *
	 * @param \Sidus\SidusBundle\Entity\Permission $authorizations
	 * @return Object
	 */
	public function addAuthorization(\Sidus\SidusBundle\Entity\Permission $authorizations) {
		$this->authorizations[] = $authorizations;

		return $this;
	}

	/**
	 * Remove authorizations
	 *
	 * @param \Sidus\SidusBundle\Entity\Permission $authorizations
	 */
	public function removeAuthorization(\Sidus\SidusBundle\Entity\Permission $authorizations) {
		$this->authorizations->removeElement($authorizations);
	}

	/**
	 * Get authorizations
	 *
	 * @return \Doctrine\Common\Collections\Collection 
	 */
	public function getAuthorizations() {
		return $this->authorizations;
	}

	public function getMetadata(){
		$metadata = [];
		$class = new \ReflectionClass($this);
		foreach($class->getMethods(\ReflectionMethod::IS_PUBLIC) as $method){
			$method = $method->name;
			if(substr($method, 0, 3) != 'get'){
				continue;
			}
			$propertyName = lcfirst(substr($method, 3));
			if(in_array($propertyName, array_merge(['title'], $this::getReservedProperties()))){
				continue;
			}
			try {
				$metadata[$propertyName] = new \Sidus\SidusBundle\Metadata\Metadata($propertyName, $this, $this->$method());
			} catch(Exception $e){
				var_dump($propertyName);
				// Nothing for now
			}
		}
		return $metadata;
	}
	
	public static function getReservedProperties(){
		return self::$reservedProperties;
	}
}
