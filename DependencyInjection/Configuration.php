<?php

namespace Sidus\SidusBundle\DependencyInjection;

use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('sidus');

        $rootNode->children()
                ->arrayNode('actions')
					->prototype('array')
						->children()
							->scalarNode('name')->defaultValue('show')->end()
							->scalarNode('permission_mask')
								->defaultValue('read')
								->validate()
								->ifNotInArray(PermissionMask::$valid_keys)
								->thenInvalid('Invalid permission mask "%s"')
								->end()
							->end()
							->scalarNode('icon')->defaultValue('icon-question')->end()
							->booleanNode('require_user')->defaultValue(false)->end()
						->end()
					->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
