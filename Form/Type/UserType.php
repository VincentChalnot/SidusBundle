<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('oldPassword', 'password', ['label' => 'Old Password', 'mapped' => false])
				->add('password', 'repeated', ['first_name' => 'new_password', 'second_name' => 'confirm', 'type' => 'password'])
				->add('email', 'email');
		
		$builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
                $form = $event->getForm();
				$user = $event->getData();
				if($user->getOldPassword() !== $user->encodePassword($form->get('oldPassword')->getData())){
					$form->get('oldPassword')->addError(new \Symfony\Component\Form\FormError('Invalid password'));
				}
			});
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'Sidus\SidusBundle\Entity\User'
		));
	}

	public function getName() {
		return 'sidusbundle_usertype';
	}

}
