<?php

namespace Sidus\SidusBundle\Controller;

use DateTime;
use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Object;
use Sidus\SidusBundle\Entity\Permission;
use Sidus\SidusBundle\Entity\Type;
use Sidus\SidusBundle\Form\Type\NodePermissionsType;
use Sidus\SidusBundle\Form\Type\PermissionType;
use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\HttpFoundation\Response;
use UnexpectedValueException;

class DefaultController extends CommonController {

	public function loginAction($node_id, $lang = null) {
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			return $this->redirect($this->generateUrl('sidus_show_node', array('node_id' => $node_id, 'lang' => $lang)));
		}

		$this->loadObjectsForNodeUID($node_id, $lang);

		if (null === $this->object) {
			if ('home' !== $node_id) {
				return $this->redirect($this->generateUrl('sidus_login', array('node_id' => 'home', 'lang' => $lang)));
			} else {
				return $this->forward('SidusBundle:Default:notFound');
			}
		}

		return $this->forward('SidusBundle:User:login', $this->loadedObjects);
	}

	/**
	 * Default action when routing to a node, loads all necessary objects and
	 * pass it to the proper controller (defined in the object's type)
	 * @param mixed $node_id
	 * @param string $lang
	 * @return Response
	 */
	public function commonAction($node_id, $lang = null) {
		$this->refreshUser();
		$this->loadObjectsForNodeUID($node_id, $lang);

		// Check if the user just logged out (the session is cleared so we can't do this in the LogoutSuccessHandler)
		if ($this->getRequestParameter('logout_success')) {
			$this->getSession()->getFlashBag()->add('info', 'Logout successfull');
			return $this->redirect($this->getPathForNode($this->node));
		}

		if (null === $this->object) {
			return $this->forward('SidusBundle:Default:notFound');
		}

		if (!$this->object->hasPermission(PermissionMask::ADMIN) && !$this->object->hasPermission($this->action->getPermissionMask())) {
			return $this->forward('SidusBundle:Default:unauthorized', $this->loadedObjects);
		}
		if ($this->action->getRequireUser() && !$this->getIsUserLoggedIn()) { //Maybe forward to show with flash message ?
			return $this->forward('SidusBundle:Default:unauthorized', $this->loadedObjects);
		}

		if ($this->getPreferedLanguage() != $this->object->getLang() && $this->action == 'show') {
			$this->getSession()->getFlashBag()->add('warning', 'Sorry, this content is not available in your language.');
		}

		$route = $this->object->getType()->getBundleName().':'.$this->object->getType()->getTypeName().':'.$this->action;
		if ($this->isValidController($route)) {
			$response = $this->forward($route, $this->loadedObjects);
			if (property_exists($response, 'loadedObjects')) {
				$this->loadedObjects = array_merge($this->loadedObjects, $response->loadedObjects);
			} else {
				return $response;
			}
		}

		if (method_exists($this, $this->action.'Action')) {
			$response = $this->{$this->action.'Action'}();
			if ($response instanceof Response) {
				return $response;
			}
		}

		return $this->render($this->getTemplateName($this->object->getType()->getBundleName(), $this->object->getType()->getTypeName(), $this->action, $this->format), $this->loadedObjects);
	}

	/**
	 * List of versions for current node
	 * @param mixed $node_id
	 * @param string $lang
	 * @return Response
	 */
	protected function historyAction() {
		$paginator = $this->get('knp_paginator');
		$paginator->reset()
				->setPage($this->getRequestParameter('page', 1))
				->setLimit(10)
				->setOption('sortFieldParameterName', 'hsort');
		$this->loadedObjects['versions'] = $this->getRepository('SidusBundle:Object')->findByReference($this->node->getObjectReference(), $paginator);
	}

	/**
	 * @todo only if POST else put the code of "choose" in here
	 * @return type
	 */
	protected function addAction() {
		$types = $this->loadTypes($this->object);
		$type = $this->getRepository('SidusBundle:Type')->findOneByTypeName($this->getRequestParameter('type'));
		$new_object = null;
		if ($type) {
			$class_name = $type->getClassName();
			if (!class_exists($class_name)) {
				$this->setFlash('error', 'Error: Unable to find object class');
			} else {
				// Initializing objects with default values
				$new_object = new $class_name;
				$new_object->setContainer($this->container);
				$new_object->setType($type);
				$new_object->setRevisionBy($this->getUserNode());
				$new_object->setCreationDate(new \DateTime);
				$new_object->setIsHead(true);
				$new_object->setReference(0); // Temporary value
			}
		}
		if (!$type || !$types->contains($type) || !$new_object) {
			$this->loadedObjects['types'] = $types;
			return $this->render("SidusBundle:Default:typeSelector.html.twig", $this->loadedObjects);
		}
		$form = $this->getFormForObject($new_object);
		$form->add('lang', 'language', [ 'label' => 'Language', 'preferred_choices' => $this->container->getParameter('locales')]);

		if ($this->getRequest()->isMethod('POST')) {
			$form->handleRequest($this->getRequest());
			if ($form->isValid()) {
				// Saving object
				$em = $this->getDoctrine()->getManager();
				$em->persist($new_object);
				$em->flush();

				// Creating node and binding object
				$node = new Node;
				$node->setContainer($this->container);
				$node->setParent($this->node);
				$node->setObjectReference($new_object->getId());
				$new_object->setReference($new_object->getId());
				$em->persist($node, $new_object);
				$em->flush();

				$this->setFlash('success', 'Your modifications have been saved');
				return $this->redirect($this->getPathForNode($node));
			}
		}
		$this->loadedObjects['form'] = $form->createView();
		$this->loadedObjects['form']->vars['label'] = "Add new {$type}";
		$this->loadedObjects['inherit_template'] = $this->getTemplateName($type->getBundleName(), $type->getTypeName(), 'edit', $this->format);
	}

	protected function editAction() {
		// Initializing object
		if ($this->object->getType()->getIsVersionned()) {
			$new_object = clone $this->object;
		} else {
			$new_object = $this->object;
		}
		$new_object->setRevisionBy($this->getUserNode());
		$new_object->setRevisionDate(new DateTime);
		$new_object->setIsHead(true);

		$form = $this->getFormForObject($new_object);

		if ($this->getRequest()->isMethod('POST')) {
			$form->handleRequest($this->getRequest());
			if ($form->isValid()) {
				if ($this->object->getType()->getIsVersionned()) {
					$this->object->setIsHead(false);
				}

				// Saving both objects
				$em = $this->getDoctrine()->getManager();
				$em->persist($new_object, $this->object);
				$em->flush();

				$this->setFlash('success', 'Your modifications have been saved');
				if (!$this->getRequestParameter('save')) {
					return $this->redirect($this->getPathForNode($this->node));
				}
			}
		}
		$this->loadedObjects['form'] = $form->createView();
		$this->loadedObjects['form']->vars['label'] = ucwords($this->object->getType()).' edition';
	}

	protected function translateAction() {
		if ($this->object->getType()->getIsVersionned()) {
			$new_object = clone $this->object;
		} else {
			$new_object = $this->object;
		}
		$form = $this->getFormForObject($new_object);

		$languages = $this->getAvailableLanguages();
		foreach ($this->translations as $translation) {
			unset($languages[$translation->getLang()]);
		}
		$form->add('lang', 'language', [ 'label' => 'Language', 'choices' => $languages, 'preferred_choices' => $this->container->getParameter('locales')]);

		if ($this->getRequest()->isMethod('POST')) {
			$form->handleRequest($this->getRequest());
			if ($form->isValid()) {
				$new_object->setRevisionBy($this->getUserNode());
				$new_object->setRevisionDate(new DateTime);
				$new_object->setIsHead(true);

				$em = $this->getDoctrine()->getManager();
				$em->detach($this->object);
				$em->persist($new_object);
				$em->flush();
				$this->setFlash('success', 'Your modifications have been saved');
				return $this->redirect($this->getPathForNode($this->node, 'show', ['lang' => $new_object->getLang()]));
			}
		}
		$this->loadedObjects['form'] = $form->createView();
		$this->loadedObjects['form']->vars['label'] = ucwords($new_object->getType()).' edition';
	}

	protected function deleteAction() {
		$this->loadDescendants($this->node);
		$this->loadObjects($this->nodes);
		$form = $this->createFormBuilder()
				->add('delete', 'hidden', ['required' => true])
				->getForm();

		if ($this->getRequest()->isMethod('POST')) {
			$form->handleRequest($this->getRequest());
			if ($form->isValid()) {
				$parent = $this->node->getParent();

				$em = $this->getDoctrine()->getManager();
				foreach ($this->descendants as $node) {
					//$em->remove($node->getObjects()); // @Todo
					$em->remove($node);
				}
				$em->remove($this->node);
				$em->flush();

				$this->getDoctrine()->getConnection() // Cleaning Object table
						->exec('DELETE FROM object WHERE reference NOT IN (SELECT object_reference FROM node)');

				$this->setFlash('success', 'The node was successfully deleted');
				return $this->redirect($this->getPathForNode($parent));
			}
		}
		$this->loadedObjects['form'] = $form->createView();
	}

	protected function permissionsAction() {
		if (!$this->object->hasPermission(PermissionMask::ADMIN) && !$this->object->hasPermission(PermissionMask::OWNER)) {
			$this->setFlash('error', 'You don\'t have the authorization for this action.');
			return $this->redirect($this->getPathForNode($this->node));
		}
		$isAdmin = $this->object->hasPermission(PermissionMask::ADMIN);
		$this->loadedObjects['isAdmin'] = $isAdmin;

		$isRedirect = $this->newPermissionAction($isAdmin);
		if ($isRedirect) {
			return $isRedirect;
		}

		$form = $this->createForm(new NodePermissionsType($isAdmin), $this->node);
		if ($this->getRequest()->isMethod('POST')) {
			$form->handleRequest($this->getRequest());
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();

				foreach ($form->get('permissions') as $permission_form) {
					if ($permission_form->has('id') && $permission_form->get('id')->isClicked()) {
						// Delete the permission
						$permission = $permission_form->getData();
						$em->remove($permission);
						$em->flush();

						$this->setFlash('success', 'The permissions was successfully deleted');
						return $this->redirect($this->getPathForNode($this->node, 'permissions'));
					}
				}

				//Saving permissions
				$em->persist($this->node);
				$em->flush();

				$this->setFlash('success', 'Your modifications have been saved');
				if (!$this->getRequestParameter('save')) {
					return $this->redirect($this->getPathForNode($this->node));
				}
			}
		}

		$this->loadedObjects['form'] = $form->createView();
	}

	/**
	 * Called inside permissionAction, separated for readability purposes
	 * @throws UnexpectedValueException
	 */
	protected function newPermissionAction($isAdmin = false) {
		$entities = [];
		foreach ($this->getRepository('SidusBundle:User')->findAll() as $entity) {
			$entities[$entity->getId()] = $entity;
		}
		$permissions = $this->getPermissionManager()->getPermissionsForNode($this->node);
		foreach ($permissions as $perm) {
			unset($entities[$perm->getEntity()->getId()]);
		}
		if ($entities) {
			$new_permission = new Permission;
			$new_permission->setNode($this->node);
			$new_permission_form = $this->createForm(new PermissionType($isAdmin), $new_permission);
			$new_permission_form->add('entity', 'choice', [ 'choices' => $entities, 'mapped' => false]);
			$new_permission_form->remove('id');
			$new_permission_form->add('id', 'submit', [ 'label' => 'Add', 'attr' => ['class' => 'btn-small btn-primary']]);

			if ($this->getRequest()->isMethod('POST')) {
				$new_permission_form->handleRequest($this->getRequest());
				$id = (int) $new_permission_form->get('entity')->getData();
				if (isset($entities[$id])) {
					$new_permission->setEntity($entities[$id]);
				}
				if ($new_permission_form->isValid()) {
					if ($new_permission->getAuthorizations()->isAdmin() && !$isAdmin) {
						throw new UnexpectedValueException('Data injected outside of form');
					}
					$em = $this->getDoctrine()->getManager();
					$em->persist($new_permission);
					$em->flush();
					$this->setFlash('success', 'New permission added');
					return $this->redirect($this->getPathForNode($this->node, 'permissions'));
				}
			}

			$this->loadedObjects['new_permission_form'] = $new_permission_form->createView();
		}
	}
	
	public function installAction() {
		$nm = $this->getNodeManager();
		$root = new \Sidus\SidusBundle\Entity\Node;
		$root;
	}

}
