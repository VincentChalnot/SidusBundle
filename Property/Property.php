<?php

namespace Sidus\SidusBundle\Property;

class GenericProperty implements PropertyInterface {

	/**
	 * The name of the property in the model
	 * @var string
	 */
	protected $name;

	/**
	 * The actual value of the property in PHP
	 * @var mixed
	 */
	protected $value;

	/**
	 * Define if property has changed (to update)
	 * @var boolean
	 */
	protected $has_changed = false;

	/**
	 * Define if the user is authorized to modify the property
	 * @var boolean
	 */
	protected $read_only;

	/**
	 * The current object associated with the property
	 * @var \StdClass
	 */
	protected $object;
	
	protected $inputType = 'text';

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::__constructor()
	 * @see http://php.net/manual/en/pdo.constants.php Documentation for PDO Params
	 * @param Sidus\Nodes\Node $node
	 * @param boolean $read_only If the user is allowed to set the value
	 * @param string $table_name Table name of the property in the Database
	 * @param string $column_name Column name of the property in the Database
	 * @param integer $pdo_type The PDO param type of the value in the DB
	 * @param string $model_name If different from column name, the PHP name
	 */
	public function __construct(\stdClass $object, $name, $read_only = true) {
		$this->object = $object;
		$this->name = $name;
		$this->read_only = (boolean) $read_only;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::get()
	 * @return mixed $value
	 */
	public function get() {
		return $this->value;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::set()
	 * @param mixed $value
	 * @return boolean
	 */
	public function set($value) {
		if (!$this->canWrite()) {
			return false; //Throw Exception ?
		}
		if ($value === $this->value) {
			return true;
		}
		if(!$this->check($value)){
			return false;
		}
		$this->value = $value;
		$this->has_changed = true;
		return true;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::hasChanged()
	 * @return boolean
	 */
	public function hasChanged() {
		return $this->has_changed;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::reset()
	 * @return boolean
	 */
	public function reset() {
		if (!$this->canWrite()) {
			return false;
		}
		$this->has_changed = false;
		return true;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::toDB()
	 * @return string $value
	 */
	public function toDB() {
		return (string) $this->value;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::check()
	 * @param mixed $value
	 * @return boolean
	 */
	public function check($value) {
		if (!$this->canWrite()) {
			return false; //Throw Exception ?
		}
		return true;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::__toString()
	 * @return string $value
	 */
	public function __toString() {
		return (string) $this->value;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::getPDOParam()
	 * @return string $value
	 */
	public function getPDOParam() {
		return $this->pdo_param;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::getInput()
	 * @return string $value
	 */
	public function getInputType() {
		return $this->input;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::setInput()
	 * @param \Sidus\HTML\Input $input
	 * @return boolean
	 */
	public function setInputType($input) {
		if ($this->canWrite()) {
			return false;
		}
		$this->input = $input;
		return true;
	}

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::canWrite()
	 * @return boolean
	 */
	public function canWrite() {
		return !$this->read_only;
	}

	/**
	 * Get the node associated to the property
	 * @return Sidus\Nodes\Node
	 */
	public function getObject() {
		return $this->object;
	}

	public function getName() {
		return $this->name;
	}

}