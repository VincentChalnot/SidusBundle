<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
			->add('typeName')
			->add('controller')
			->add('authorized_types','entity', array(
				'class'=>'SidusBundle:Type',
				'property'=>'title',
				'expanded'=>true,
				'multiple'=>true,
			))
			->add('forbidden_types','entity', array(
				'class'=>'SidusBundle:Type',
				'property'=>'title',
				'expanded'=>true,
				'multiple'=>true,
			))
			->add('versioning', 'checkbox')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sidus\SidusBundle\Entity\Type'
        ));
    }

    public function getName()
    {
        return 'sidusbundle_typetype';
    }
}