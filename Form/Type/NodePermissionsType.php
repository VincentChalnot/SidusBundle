<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NodePermissionsType extends AbstractType {
	
	protected $isAdmin;


	public function __construct($isAdmin = false) {
		$this->isAdmin = $isAdmin;
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('permissions', 'collection', ['type' => new PermissionType($this->isAdmin)])
			->add('inheritPermissions', 'checkbox', [ 'required' => false, 'label' => 'Inherit permissions from parent' ]);
		
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $form = $event->getForm();
				if(!$event->getData()->getParent()){
					$form->remove('inheritPermissions');
				}
			});
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'Sidus\SidusBundle\Entity\Node'
		));
	}

	public function getName() {
		return 'sidusbundle_nodepermissionstype';
	}

}