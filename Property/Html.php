<?php

namespace Sidus\SidusBundle\Property;

use Exception;
use Sidus\SidusBundle\Lib\Utils;

class Html extends Text {

	/**
	 * @see Sidus\SidusBundle\Property\PropertyInterface::check()
	 * @param string $value
	 * @return boolean
	 */
	public function check($value) {
		try {
			$value = (string) $value;
		} catch (Exception $e) {
			return false;
		}
		return true;
	}

	/**
	 * Escape the sting properly to display in HTML content
	 * @see Sidus\SidusBundle\Property\PropertyInterface::__toString()
	 * @return string $value
	 */
	public function __toString() {
		return (string) $this->value;
	}

}
