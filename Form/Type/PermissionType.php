<?php

namespace Sidus\SidusBundle\Form\Type;

use Sidus\SidusBundle\Permission\PermissionMask;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PermissionType extends AbstractType {
	
	protected $isAdmin;
	
	public function __construct($isAdmin = false) {
		$this->isAdmin = $isAdmin;
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('id', 'submit', [ 'label' => 'Remove', 'attr' => ['class' => 'btn-small btn-warning', 'value' => 'remove'] ]);
		foreach(['read', 'write', 'add', 'delete', 'owner', 'admin'] as $key){
			$builder->add('allow'.ucfirst($key), 'checkbox', [ 'required' => false, 'label' => ' ' ]);
			$builder->add('forbid'.ucfirst($key), 'checkbox', [ 'required' => false, 'label' => ' ' ]);
		}
		if(!$this->isAdmin){
			$builder->remove('allowAdmin')
					->remove('forbidAdmin');
			$builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $form = $event->getForm();
				if($event->getData()->getAuthorizations()->isAdmin()){
					$form->remove('id');
					foreach(['read', 'write', 'add', 'delete', 'owner', 'admin'] as $key){
						$form->remove('allow'.ucfirst($key));
						$form->remove('forbid'.ucfirst($key));
					}
				}
			});
		}
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'Sidus\SidusBundle\Entity\Permission'
		));
	}

	public function getName() {
		return 'sidusbundle_permissiontype';
	}

}