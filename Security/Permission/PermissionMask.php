<?php

namespace Sidus\SidusBundle\Permission;

class PermissionMask {
	const READ = 1;
	const WRITE = 2;
	const ADD = 4;
	const DELETE = 8;
	const OWNER = 31;
	const ADMIN = 47;
	const ALL = 63;
	
	public static $valid_keys = [ 'read', 'show', 'write', 'edit', 'add', 'delete', 'remove', 'owner', 'admin', 'master', 'all' ];
	public static $masks = [
		'read' => self::READ,
		'show' => self::READ, // Alias
		'write' => self::WRITE,
		'edit' => self::WRITE, // Alias
		'add' => self::ADD,
		'delete' => self::DELETE,
		'remove' => self::DELETE, //Alias
		'owner' => self::OWNER,
		'admin' => self::ADMIN,
		'master' => self::ADMIN, //alias
		'all' => self::ALL,
	];

	protected $mask;

	/**
	 *
	 * @param int $mask
	 */
	public function __construct($mask = 0) {
		if(is_string($mask) && in_array($mask, self::$valid_keys)){
			$mask = self::$masks[$mask];
		}
		if($mask instanceof self){
			$mask = $mask->getMask();
		}
		if(($mask & 16) === 16 || ($mask & 32) === 32){
			$mask = $mask | 15;
		}
		$this->mask = $mask;
	}

	public function check($permission = self::READ){
		$mask = new self($permission);
		return ($this->mask & $mask->getMask()) == $mask->getMask();
	}

	public function canRead(){
		return $this->check(self::READ);
	}

	public function canWrite(){
		return $this->check(self::WRITE);
	}

	public function canAdd(){
		return $this->check(self::ADD);
	}

	public function canDelete(){
		return $this->check(self::DELETE);
	}

	public function isOwner(){
		return $this->check(self::OWNER);
	}

	public function isAdmin(){
		return $this->check(self::ADMIN);
	}

	public function getMask(){
		return $this->mask;
	}

}
