<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder->add('title', 'text', ['attr' => ['class' => 'inline-header', 'placeholder' => 'Title']])
				->add('content', 'inlineeditable', ['attr' => ['class' => 'content'], 'required' => false])
				->add('author', 'entity', [ 'class' => 'SidusBundle:User', 'required' => false ])
				->add('description', 'textarea', [ 'required' => false ])
				->add('tags', 'text', ['required' => false]);
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver) {
		$resolver->setDefaults(array(
			'data_class' => 'Sidus\SidusBundle\Entity\Object'
		));
	}

	public function getName() {
		return 'sidusbundle_pagetype';
	}

}