<?php

namespace Sidus\SidusBundle\Handler;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class LoginFailureHandler implements AuthenticationFailureHandlerInterface {

	protected $router;

	public function __construct(Router $router) {
		$this->router = $router;
	}

	public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
		$request->getSession()->set(SecurityContext::AUTHENTICATION_ERROR, $exception);
		return new RedirectResponse($this->router->generate('sidus_login', array('node_id' => $request->get('node_id'))));
	}

}