<?php

namespace Sidus\SidusBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class RichTextareaType extends AbstractType {

	/**
	 * {@inheritdoc}
	 */
	public function buildView(FormView $view, FormInterface $form, array $options) {
		$view->vars['pattern'] = null;
		$view->vars['attr']['class'] = 'rich_textarea';
		//$view->vars['attr']['contenteditable'] = 'true';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getParent() {
		return 'textarea';
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName() {
		return 'richtextarea';
	}

}
