<?php

namespace Sidus\SidusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sidus\SidusBundle\Entity\Node;
use Sidus\SidusBundle\Entity\Object;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Sidus\SidusBundle\Entity\PageRepository")
 */
class Page extends Object {

	/**
	 * @var string
	 * @ORM\Column(name="content", type="text", nullable=true)
	 */
	private $content;

	/**
	 * @var string
	 * @ORM\Column(name="tags", type="text", nullable=true)
	 */
	private $tags;

	/**
	 * @ORM\ManyToOne(targetEntity="Sidus\SidusBundle\Entity\Node")
	 * @ORM\JoinColumn(name="author_id", nullable=true)
	 */
	private $author;

	/**
	 * @var string
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	public function __construct() {
		parent::__construct();
		$this->content = '';
		$this->tags = '';
	}

	/**
	 * @param string $content
	 * @return Page
	 */
	public function setContent($content) {
		$this->enforcePermision(__FUNCTION__);
		$this->content = (string) $content;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContent() {
		return $this->checkPermission(__FUNCTION__, $this->content);
	}

	/**
	 * @param string $tags
	 * @return Page
	 */
	public function setTags($tags) {
		$this->enforcePermision(__FUNCTION__);
		$this->tags = (string) $tags;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTags() {
		return $this->checkPermission(__FUNCTION__, $this->tags);
	}

	public function getFormType() {
		return '\Sidus\SidusBundle\Form\Type\PageType';
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Page
	 */
	public function setDescription($description) {
		$this->description = (string)$description;
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string 
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set author
	 *
	 * @param Node $author
	 * @return Page
	 */
	public function setAuthor($author = null) {
		if ($author instanceof Object) {
			$author = $author->getCurrentNode();
		}
		$this->author = $author;
		return $this;
	}

	/**
	 * Get author
	 *
	 * @return \Sidus\SidusBundle\Entity\Node 
	 */
	public function getAuthor() {
		return $this->author;
	}

}
