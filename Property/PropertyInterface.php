<?php

namespace Sidus\SidusBundle\Property;

/**
 * Common interface for all properties, define an set of rules to exchange vars
 * from Database to the end user interface.
 * All methods must check the permission set of the user before doing anything.
 * @see Sidus\SidusBundle\Property\GenericProperty
 */
interface PropertyInterface {

	/**
	 * Set the value for the first time where $value is a correct PHP type
	 * @see http://php.net/manual/en/pdo.constants.php Documentation for PDO Params
	 * @param string $table_name Table name of the property in the Database
	 * @param string $column_name Column name of the property in the Database
	 * @param integer $pdo_type The PDO param type of the value in the DB
	 * @param string $model_name If different from column name, the PHP name
	 */
	public function __construct(\Sidus\Nodes\Node $node, $table_name, $column_name, $read_only = true, $model_name = null);

	/**
	 * Return the value for PHP with correct type and no processing for display
	 * @return mixed $value
	 */
	public function get();

	/**
	 * Set the value of the property where $value is a correct PHP type
	 * Call $this->check($value) to ensure correct input and can throw an error
	 * Must change $this->has_changed to true;
	 * @param mixed $value
	 * @return boolean
	 */
	public function set($value);

	/**
	 * Check if the value has been changed since it's first assignment
	 * @return boolean
	 */
	public function hasChanged();

	/**
	 * Reset the hasChanged property to false
	 * @return null
	 */
	public function reset();

	/**
	 * Return a variable for binding params to prepared statements
	 * @return mixed $value
	 */
	public function toDB();

	/**
	 * Check if the $value is acceptable for this property
	 * @param $value
	 * @return boolean
	 */
	public function check($value);

	/**
	 * The default output formatting of the variable
	 * @return string $value 
	 */
	public function __toString();

	/**
	 * Returns the model name of the property
	 * @return string $name 
	 */
	public function getName();

	/**
	 * Returns the default input object for forms for this property
	 * @return string $value 
	 */
	public function getInputType();

	/**
	 * Set the default input object for forms for this property
	 * @param $input
	 */
	public function setInputType($input);

	/**
	 * Check if permission set can write to value
	 * @return boolean
	 */
	public function canWrite();

}
